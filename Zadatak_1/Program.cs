﻿using System;

namespace Zadatak_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1 = new Note("One", "note1");
            Note note2 = new Note("Two", "note2");
            Note note3 = new Note("Three", "note3");
            Note note4 = new Note("Four", "note4");
            Note note5 = new Note("Five", "note5");

            Notebook notebook = new Notebook();
  
            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);
            notebook.AddNote(note4);
            notebook.AddNote(note5);
            IAbstractIterator iterator = notebook.GetIterator();
            for(int i =0;i<notebook.Count;i++)
            {
                iterator.Current.Show();
                iterator.Next();
            }

            Console.WriteLine("Note count: "+ notebook.Count);
            notebook.RemoveNote(note3);
            Console.WriteLine("Note count: "+ notebook.Count);
            notebook.Clear();
            Console.WriteLine("Note count: " + notebook.Count);
        }
    }
}
