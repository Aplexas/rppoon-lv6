﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_1
{
    class Notebook : IAbstractCollection
    {
        private List<Note> notes;
        public Notebook()
        {/////////////
            this.notes = new List<Note>();
        }
            public Notebook(List<Note> notes)
            {
                this.notes = new List<Note>(notes.ToArray());
            }
            public void AddNote(Note note)
            { //implementation missing
                notes.Add(note);
            }
            public void RemoveNote(Note note)
            { //implementation missing! 
                foreach(Note temp_note in notes)
                {
                    if (temp_note==note)
                    {
                        notes.Remove(temp_note);
                        Console.WriteLine("Removed: " + temp_note.Title);
                        return;
                    }
                }
                Console.WriteLine(note.Title+" is not in list of notes");

            }
            public void Clear()
            { 
            this.notes.Clear();
            Console.WriteLine("Cleared list of notes");
           
            }
            
 public int Count { get { return this.notes.Count; } }
        public Note this[int index] { get { return this.notes[index]; } }
        public IAbstractIterator GetIterator() { return new Iterator(this); }
    }
}
