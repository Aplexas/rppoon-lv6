﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_3
{
    class CareTaker
    {
        private List<Memento> previousStates;
        private int numberOfPreviousState;

        public CareTaker()
        {
            this.previousStates = new List<Memento>();
            this.numberOfPreviousState = 0;
        }


        public Memento getPreviousState()
        {
            if (numberOfPreviousState != 0) {

                numberOfPreviousState -= 1;
            }
            
            return this.previousStates[numberOfPreviousState];
        }

        public void setPreviousState(Memento memento)
        {
            this.previousStates.Add(memento);
            numberOfPreviousState++;

        }
        public int Count { get { return this.previousStates.Count; } }
        public Memento this[int index] { get { return this.previousStates[index]; } }

    }
}
