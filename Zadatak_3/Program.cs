﻿using System;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            System.DateTime date1 = new System.DateTime(2025, 3, 10, 2, 15, 10);
            System.DateTime date2 = new System.DateTime(2025, 7, 15, 6, 30, 20);
            ToDoItem toDoItem1 = new ToDoItem("One", "test", date1);

            CareTaker careTaker = new CareTaker();

            careTaker.setPreviousState(toDoItem1.StoreState());
            Console.WriteLine(toDoItem1.ToString());

            toDoItem1.Rename("Changed");
            careTaker.setPreviousState(toDoItem1.StoreState());
            Console.WriteLine(toDoItem1.ToString());

            toDoItem1.ChangeTask("testchanged");
            Console.WriteLine(toDoItem1.ToString());

            toDoItem1.RestoreState(careTaker.getPreviousState());
            Console.WriteLine(toDoItem1.ToString());
            toDoItem1.RestoreState(careTaker.getPreviousState());
            Console.WriteLine(toDoItem1.ToString());

        }
    }
}
