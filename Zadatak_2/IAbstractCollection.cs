﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_2
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
