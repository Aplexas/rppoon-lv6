﻿using System;

namespace Zadatak_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product item1 = new Product("One", 5);
            Product item2 = new Product("Two", 5);
            Product item3 = new Product("Three", 5);

            Box box = new Box();

            box.AddProduct(item1);
            box.AddProduct(item2);
            box.AddProduct(item3);
            IAbstractIterator iterator = box.GetIterator();
            for (int i = 0; i < box.Count; i++)
            {
                Console.WriteLine(iterator.Current.ToString());
                iterator.Next();
            }

        }
    }
}
